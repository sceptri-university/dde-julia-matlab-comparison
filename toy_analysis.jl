using Revise
using DifferentialEquations, Plots

function dde_system(dy, y, h, p, t)
	a, b, τ = p
	yₜ = h(p, t - τ)

	dy[1] = a * y[1] * (1 - b*yₜ[1]) + exp(-y[1]^2)
end

function do_experiment(τ)
    lags = [τ]
    h(p, t) = ones(1)

    tspan = (0, 10000)
    u₀ = [1]

    p = [1.5, 0.1, τ]

    alg = MethodOfSteps(BS3())
    dde_problem = DDEProblem(dde_system, u₀, h, tspan, p; constant_lags=lags)

    return solve(dde_problem, alg)
end

# Compilation run
do_experiment(1);

print("tau = 1:")
@time do_experiment(1);
print("tau = 2:")
@time do_experiment(2);