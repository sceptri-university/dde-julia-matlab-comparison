let
  pkgs = import <nixpkgs> {};
  unstablePkgs = import <nixos-unstable> {};
  lib = import <lib> {};
  config = import <config> {};

  configPath = /home/sceptri/Documents/Dev/homelab/.;
in
  pkgs.mkShell {
    buildInputs = with pkgs; [
      (unstablePkgs.julia.withPackages [
        "Revise"
        "Plots"
        "DifferentialEquations"
      ])
    ];
  }
