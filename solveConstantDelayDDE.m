function sol = solveConstantDelayDDE(f, dde_tspan, delta_t, hist, lag, ode_solver)
    % Define your DDE function f(t, y, z)
    
    % Set up time span
    tspan = dde_tspan(1):delta_t:dde_tspan(end);
    
    % If delta t is less then lag, shift it at least 1 iteration back
    index_delay = int32(lag / delta_t);
    if index_delay <= 0
        index_delay = 1;
    end
    
    % Initialize the solution vectors
    y = zeros(length(hist), length(tspan));
    ode_steps = zeros(size(tspan));
    y(:, 1) = hist(:);

    % Implement the Method of Steps
    for i = 2:length(tspan)
        i_delayed = i - index_delay;
        if i_delayed < 1
            i_delayed = 1;
        end
        
        % Solve the ODE using the specified ODE solver
        [Ts, y_ode] = feval(ode_solver, @(t, x) f(t, x, y(:, i_delayed)), [tspan(i-1), tspan(i)], y(:, i-1));

        % Update the solution vector
        y(:, i) = y_ode(end, :);
        ode_steps(i) = length(Ts);
    end

    sol = struct('x', tspan, 'y', y, 'solver', ode_solver, 'ode_steps', ode_steps);
end