clc
clear

T_max = 10000;

disp("Native: ")
disp("tau = 1: ")
tic
do_experiment(1, T_max);
toc
disp("tau = 2: ")
tic
do_experiment(2, T_max);
toc

disp("Custom: ")
disp("tau = 1: ")
tic
do_experiment_custom(1, T_max);
toc
disp("tau = 2: ")
tic
do_experiment_custom(2, T_max);
toc

% ::: Definitions
function sol = do_experiment(tau, T_max)
	lags = [tau];
	hist = [1];
	tspan = [0, T_max];

	sol = dde23(@dde_system, lags, hist, tspan);
end

function sol = do_experiment_custom(tau, T_max)
	lags = [tau];
	hist = [1];
	tspan = [0, T_max];
	dt = 0.01;

	sol = solveConstantDelayDDE(@dde_system, tspan, dt, hist, lags(1), @ode23);
end

% Define a toy model (delayed logistic equation + some exp)
function ydot = dde_system(~, y, ydelay)
    a = 1.5;
    b = 0.1;
    ydot = a*y*(1 - b*ydelay) + exp(-y^2);
end