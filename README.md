# DDE solver comparison between Matlab and Julia

## Setup

Here we compare delay differential equation solvers, namely DiffEqDelay.jl (which is a part of DifferentialEquations.jl) and a native `dde_23` Matlab solver. With Julia, we use a method of steps with `BS3` (Bogacki-Shampine 3/2 method) ODE solver (equivalent Julia solver to Matlab `ode23`, see [DifferentialEquations.jl docs](https://docs.sciml.ai/DiffEqDocs/stable/solvers/ode_solve/#Translations-from-MATLAB/Python/R)).

The following results were run for Matlab R2023a and Julia 1.10.0 with `versioninfo()`:
```
Platform Info:
  OS: Linux (x86_64-unknown-linux-gnu)
  CPU: 8 × Intel(R) Core(TM) i7-8565U CPU @ 1.80GHz
  WORD_SIZE: 64
  LIBM: libopenlibm
  LLVM: libLLVM-15.0.7 (ORCJIT, skylake)
  Threads: 1 on 8 virtual cores
Environment:
  JULIA_CONDAPKG_OFFLINE = yes
  JULIA_CONDAPKG_BACKEND = Null
  JULIA_LOAD_PATH = @:/nix/store/cy7cb9fzsbrc19ry0b0hslx4ia9nblyy-julia-depot/project/Project.toml:@v#.#:@stdlib
  JULIA_PYTHONCALL_EXE = @PyCall
  JULIA_DEPOT_PATH = /home/sceptri/.julia:/nix/store/cy7cb9fzsbrc19ry0b0hslx4ia9nblyy-julia-depot/depot
  JULIA_PROJECT = /nix/store/cy7cb9fzsbrc19ry0b0hslx4ia9nblyy-julia-depot/project
  JULIA_NUM_THREADS = 
  JULIA_EDITOR = code
```

> The julia code can be run in a shell provided by `shell.nix` (via `nix-shell shell.nix` using the Nix package manager)

As a note, there's also the `solveConstantDelayDDE.m` file, which should be a naive implementation of the method of steps using an arbitrary ODE solver. While I had some luck getting better results in the case of 2 coupled Interneuron models, with this toy model, the results were slightly worse (keep in mind, though, that it is not optimized in any way).

## Methodology

We used the following toy model (perturbed delayed logistic equation)
$$
y'(t) = a \cdot y(t) (1 - b \cdot y(t - \tau)) + e^{-y^2(t)}
$$
where $a = 1.5, b = 0.1$ and $\tau = 1$ or $\tau = 2$ depending on the simulation. Moreover, we computed the solution on the timespan $(0, 10000)$. The initial condition was $y(0) = 1$ and history function was constant $1$, i.e. $h(p, t- \tau) \equiv 1$.

## Results

Using Matlab I get the following results:
```
Native: 
tau = 1: 
Elapsed time is 1.875005 seconds.
tau = 2: 
Elapsed time is 41.470750 seconds.
```
while the equivalent Julia code gives
```
tau = 1:  0.017200 seconds (121.78 k allocations: 8.328 MiB)
tau = 2:  0.092981 seconds (820.95 k allocations: 61.969 MiB)
```